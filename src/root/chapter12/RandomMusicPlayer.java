package root.chapter12;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;

/**
 * Created by ove on 25.06.2014.
 */
public class RandomMusicPlayer {

    static JFrame frame = new JFrame("My first music app");
    static MyDrawPanel myDrawPanel;

    public static void main(String[] args) {
        RandomMusicPlayer player = new RandomMusicPlayer();
        player.play();
    }

    private void play() {
        setUpGui();

        try{
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequencer.addControllerEventListener(myDrawPanel, new  int[] {127});
            Sequence sequence = new Sequence(Sequence.PPQ, 4);
            Track track = sequence.createTrack();
            
            int r = 0;
            for (int i = 0; i < 240; i += 4) {
                r = (int) ((Math.random() *50) +1);
                track.add(makeEvent(144,1,r,100,i));
                track.add(makeEvent(176,1,127,0,i));
                track.add(makeEvent(128,1,r,100,i+4));
            }

            sequencer.setSequence(sequence);
            sequencer.start();
            sequencer.setTempoInBPM(160);

        } catch (MidiUnavailableException e) {
            e.printStackTrace();
        } catch (InvalidMidiDataException e) {
            e.printStackTrace();
        }
    }

    private MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try{
            ShortMessage message = new ShortMessage();
            message.setMessage(comd, chan, one, two);
            event = new MidiEvent(message, tick);
        } catch (InvalidMidiDataException e) {
            e.printStackTrace();
        }
        return event;
    }

    private void setUpGui() {
        myDrawPanel = new MyDrawPanel();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(myDrawPanel);
        frame.setBounds(30,30,300,300);
        frame.setVisible(true);
    }

    class MyDrawPanel extends JPanel implements ControllerEventListener{

        boolean msg = false;

        @Override
        public void controlChange(ShortMessage event) {
            msg = true;
            repaint();
        }

        public void paintComponent(Graphics g){
            if (msg){
                Graphics2D g2 = (Graphics2D) g;

                int r = (int) (Math.random() * 250);
                int gr = (int) (Math.random() * 250);
                int b = (int) (Math.random() * 250);

                g.setColor(new Color(r, gr, b));

                int ht = (int) ((Math.random() *120) +10);
                int width = (int) ((Math.random() *120) +10);

                int x = (int) ((Math.random() *100) +10);
                int y = (int) ((Math.random() *100) +10);

                g.fillOval(x,y,ht,width);
                msg = false;
            }
        }
    }
}
