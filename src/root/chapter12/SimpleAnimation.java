package root.chapter12;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ove on 25.06.2014.
 */
public class SimpleAnimation {

    int x = 70;
    int y = 70;

    public static void main(String[] args) {
        SimpleAnimation gui = new SimpleAnimation();
        gui.draw();
    }

    private void draw() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        MyDrawPanel drawPanel = new MyDrawPanel();

        frame.getContentPane().add(drawPanel);
        frame.setSize(300,300);
        frame.setVisible(true);

        for (int i = 0; i < 100; i++) {
            x++;
            y++;
            drawPanel.repaint();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    class MyDrawPanel extends JPanel{
        public void paintComponent(Graphics g){
            g.fillRect(0,0,this.getWidth(),this.getHeight());
            g.setColor(Color.BLUE);
            g.fillOval(x,y,40,40);
        }
    }
}
