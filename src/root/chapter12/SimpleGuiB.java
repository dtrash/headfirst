package root.chapter12;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ove on 25.06.2014.
 */
public class SimpleGuiB implements ActionListener{

    JButton button;

    public static void main(String[] args) {
        SimpleGuiB frame = new SimpleGuiB();
        frame.go();
    }

    public void go(){
        JFrame frame = new JFrame();
        button = new JButton("Press me");
        button.addActionListener(this);

        frame.getContentPane().add(button);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        button.setText("I have been pressed");
    }
}
