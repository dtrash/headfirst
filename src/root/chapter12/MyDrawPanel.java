package root.chapter12;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ove on 25.06.2014.
 */
public class MyDrawPanel extends JPanel{
    @Override
    public void paintComponent(Graphics g){
        g.fillRect(0,0, this.getWidth(), this.getHeight());

        int red = (int) (Math.random() * 255);
        int green = (int) (Math.random() * 255);
        int blue = (int) (Math.random() * 255);

        Color myColor = new Color(red, green, blue);
        g.setColor(myColor);

        float u = 0, r = 100, a = 70;
        int x = this.getWidth() / 2;
        int y = this.getHeight() / 2;

        while (u <= 2 * Math.PI){
            x += Math.round(r * Math.cos(u));
            y -= Math.round(r * Math.sin(u));
            g.fillOval(x,y, Math.round(a), Math.round(a));
            u = (float) (u + 0.5);
            a = (float) (a - 0.03);
        }
        //g.fillOval(70,70,100,100);
        //g.
    }
}
