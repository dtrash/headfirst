package root.chapter10;

import java.util.Calendar;

/**
 * Created by ove on 25.06.2014.
 */
public class CalendarExamples {
    public static void main(String[] args) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(2014, 5, 25, 0, 2);
        long day1 = calendar.getTimeInMillis();
        day1 += 1000 * 60 * 60 *8;
        calendar.setTimeInMillis(day1);
        System.out.println(" Plus 8 hours " + calendar.get(calendar.HOUR_OF_DAY));
        calendar.add(calendar.DATE, 45);
        System.out.println(" Plus 45 days " + calendar.getTime());
        calendar.roll(calendar.DATE, 40);
        System.out.println(" Roll 40 days " + calendar.getTime());
        calendar.set(calendar.DATE, 1);
        System.out.println(calendar.getTime());
    }
}
