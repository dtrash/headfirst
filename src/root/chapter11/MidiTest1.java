package root.chapter11;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;

/**
 * Created by ove on 25.06.2014.
 */
public class MidiTest1 {
    public void play(){
        try{
            Sequencer sequencer = MidiSystem.getSequencer();
            System.out.println("Секвенсор создан");
        } catch (MidiUnavailableException e) {
            System.out.println("При инциализщации секвенсора произошла ошибка");
        }
    }

    public static void main(String[] args) {
        MidiTest1 midiTest1 = new MidiTest1();
        midiTest1.play();
    }
}
