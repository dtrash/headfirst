package root.chapter11;

import javax.sound.midi.*;

/**
 * Created by ove on 25.06.2014.
 */
public class MiniMusicApp {
    public static void main(String[] args) {
        MiniMusicApp miniMusicApp = new MiniMusicApp();
        miniMusicApp.play();
    }

    public void play(){
        try {
            Sequencer player = MidiSystem.getSequencer();
            player.open();
            Sequence sequence = new Sequence(Sequence.PPQ, 4);
            Track track = sequence.createTrack();

            ShortMessage note = new ShortMessage();
            note.setMessage(144, 1, 44, 100);
            MidiEvent noteOn = new MidiEvent(note, 1);
            track.add(noteOn);

            ShortMessage note2 = new ShortMessage();
            note.setMessage(128, 1, 44, 100);
            MidiEvent noteOff = new MidiEvent(note2, 16);
            track.add(noteOff);

            player.setSequence(sequence);
            player.start();
        //    player.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
